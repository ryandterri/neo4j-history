USE [LabsDataStore]
GO

/****** Object:  View [dbo].[Upcoming_Job_Report]    Script Date: 4/16/2015 12:15:20 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[Upcoming_Job_Report]
AS
SELECT        j.JobId, j.JobNumberDisplay AS JobNumber, j.ProjectName, j.EstimateStartDate AS StartDate, j.StatusId, js.Status, c.CompanyId, c.Name AS Company, 
                         ISNULL(c.ShortName, '') AS ShortName, l.LocationId, l.LocationName AS Location, MAX(u.UserId) AS SalesRepId, MAX(u.FirstName) + ' ' + MAX(u.LastName) 
                         AS SalesRep, CAST(CASE WHEN SUM(CASE WHEN s.OnOffShore = 'OFFSHORE' THEN 1 ELSE 0 END) > 0 THEN 0 ELSE 1 END AS bit) AS Offshore, 
                         CAST(CASE WHEN SUM(CASE WHEN b.BidId IS NULL THEN 0 ELSE 1 END) > 0 THEN 1 ELSE 0 END AS bit) AS HasBid, 
                         SUM(ISNULL(bi.Price * bi.Quantity * CASE bi.days WHEN 0 THEN 1 ELSE isnull(bi.days, 1) END, 0)) AS Revenue
FROM            dbo.Job AS j INNER JOIN
                         dbo.JobStatus AS js ON j.StatusId = js.JobStatusId INNER JOIN
                         dbo.CompanyLocation AS cl ON j.CompanyLocationId = cl.CompanyLocationId INNER JOIN
                         dbo.Company AS c ON cl.CompanyId = c.CompanyId INNER JOIN
                         dbo.Location AS l ON j.LocationId = l.LocationId LEFT OUTER JOIN
                         dbo.JobAssignment AS ja ON j.JobId = ja.JobId AND ja.IsPrimary = 1 LEFT OUTER JOIN
                         dbo.UserRole AS ur ON ja.UserRoleId = ur.UserRoleId AND ur.RoleId = 2 LEFT OUTER JOIN
                         dbo.[User] AS u ON ur.UserId = u.UserId LEFT OUTER JOIN
                         dbo.JobSite AS jos ON j.JobId = jos.JobId LEFT OUTER JOIN
                         dbo.Site AS s ON jos.SiteId = s.SiteId LEFT OUTER JOIN
                         dbo.Bid AS b ON j.JobId = b.JobId LEFT OUTER JOIN
                         dbo.BidTab AS bt ON b.BidId = bt.BidId LEFT OUTER JOIN
                         dbo.BidGroup AS bg ON bt.Id = bg.BidTabId LEFT OUTER JOIN
                         dbo.BidItem AS bi ON bg.Id = bi.BidGroupId
WHERE        (j.StatusId IN (4, 14))
GROUP BY j.JobId, j.JobNumberDisplay, j.ProjectName, j.EstimateStartDate, j.StatusId, js.Status, c.CompanyId, c.Name, c.ShortName, l.LocationId, l.LocationName

GO


