
--Tag migration for wellsitecorehandlingtags

update job_tag_type set name = 'Stabalization' where name = 'Stabilization';
delete from job_tag;
insert into job_tag
select
  w.JobId,
  (select jtt.Id from Job_Tag_Type jtt where rtrim(ltrim(replace(jtt.Name, ' ', ''))) = ltrim(rtrim(replace(tag.name, ' ', '')))) as JobTagTypeId,  
  case 
    when t.SecondaryValue is not null then 1    
	else NULL
  end as Quantity,
  case
    when t.SecondaryValue is not null then 'per 10 ft'
	else NULL
  end as unit
from 
  wft.WellSiteCoreHandlingTag t inner join
  wft.WellSiteCoreHandling c on t.WellSiteCoreHandlingId = c.WellSiteCoreHandlingId inner join
  wft.Tag tag on t.TagId = tag.TagId inner join
  wft.TagType tt on tag.TagTypeId = tt.TagTypeId inner join
  wft.WellSite w on c.WellSiteId = w.WellSiteId;
update job_tag_type set name = 'Stabilization' where name = 'Stabalization';


--Tag migration for total length
insert into Job_Tag
select 
  w.JobId,
  57,
  c.TotalLength as Quantity,
  case 	
	when c.TotalLengthUnit is null then 'FT'
	when c.TotalLengthUnit = '0' then 'FT'
	else c.TotalLengthUnit
  end as Unit
from
  wft.WellSite w
  inner join wft.WellSiteCoreHandling c on w.WellSiteId = c.WellSiteId
where
  c.TotalLength is not null
--Tag migration for Core Diameter
insert into Job_Tag
select 
  w.JobId,
  58,
  c.CoreDiameter as Quantity,  
  case 	
	when c.CoreDiameterUnit is null then 'IN'
	when c.CoreDiameterUnit = '0' then 'IN'
	else c.CoreDiameterUnit
  end as Unit
from
  wft.WellSite w
  inner join wft.WellSiteCoreHandling c on w.WellSiteId = c.WellSiteId
where
  c.CoreDiameter is not null