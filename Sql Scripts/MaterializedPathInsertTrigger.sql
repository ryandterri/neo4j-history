USE [LabsDataStoreDev]
GO
/****** Object:  Trigger [dbo].[trg_Job_Tag_Type_Item_Insert]    Script Date: 4/24/2015 9:41:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[trg_Job_Tag_Type_Item_Insert] on [dbo].[Job_Tag_Type_Item] after insert as
begin
	declare @id int, @parentid int, @name varchar(50), @depth int, @lineage varchar(255);
	select @id = id, @parentid = parentid, @name = name from inserted;
	if (@parentid >= 0)
	begin
		select @depth = depth+1, @lineage = lineage from Job_Tag_Type_Item where id = @parentid;
		update Job_Tag_Type_Item 
		set depth = @depth, lineage = @lineage + cast(@id as varchar) + '/'
		where id = @id;
	end
	else
	begin
		update Job_Tag_Type_Item
		set depth = 0, lineage = '/' + cast(i.id as varchar) + '/'
		from Job_Tag_Type_Item jtti join inserted as i
		on jtti.id = i.id;
	end
end

