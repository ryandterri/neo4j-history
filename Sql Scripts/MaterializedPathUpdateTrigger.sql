USE [LabsDataStoreDev]
GO
/****** Object:  Trigger [dbo].[trg_Job_Tag_Type_Item_Update]    Script Date: 4/24/2015 9:50:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[trg_Job_Tag_Type_Item_Update] on [dbo].[Job_Tag_Type_Item] after update as
begin
	declare @id int, @parentid int, @originallineage varchar(255), @originaldepth int, @depth int = 0, @lineage varchar(255);
	select @id = id, @parentid = parentid, @originallineage = lineage, @originaldepth = depth from inserted;
	if (@parentid >= 0)
	begin
		select @depth = depth+1, @lineage = lineage + cast(@id as varchar) + '/' from Job_Tag_Type_Item where id = @parentid;
		update Job_Tag_Type_Item 
		set depth = @depth, lineage = @lineage
		where id = @id;
		
		update Job_Tag_Type_Item
		set lineage = REPLACE(lineage, @originallineage, @lineage), depth = depth + (@depth - @originaldepth)
		where lineage like @originallineage + '%';
	end
	else
	begin
		update Job_Tag_Type_Item
		set depth = 0, lineage = '/' + cast(jtti.id as varchar) + '/'
		from Job_Tag_Type_Item jtti join inserted as i
		on jtti.id = i.id;
	end
end

